<?php
require('config.php');

$curl = curl_init();
$fileName = "miletech.json";
$fp = fopen($fileName, "w");

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.milletech.se/invoicing/export/customers",
  CURLOPT_FILE => $fp,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "postman-token: edd7736d-6918-bc47-fdc5-ef6b442fbe2a"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

fclose($fp);

$jsondata = file_get_contents($fileName);
$data = json_decode($jsondata, true);

foreach ($data as $row) {
  $email = $row['email'];
  $firstname = $row['firstname'];
  $lastname = $row['lastname'];
  $gender = $row['gender'];
  $customer_activated = $row['customer_activated'];
  $group_id = $row['group_id'];
  $customer_company = $row['customer_company'];
  $default_billing = $row['default_billing'];
  $default_shipping = $row['default_shipping'];
  $is_active = $row['is_active'];
  $created_at = $row['created_at'];
  $updated_at = $row['updated_at'];
  $customer_invoice_email = $row['customer_invoice_email'];
  $customer_extra_text = $row['customer_extra_text'];
  $customer_due_date_period = $row['customer_due_date_period'];
  $id = $row['id'];

  $address = $row['address'];
  $address_id = $row['address']['id'];
  $customer_id = $row['address']['customer_id'];
  $customer_address_id = $row['address']['customer_address_id'];
  $address_email = $row['address']['email'];
  $address_firstname = $row['address']['firstname'];
  $address_lastname = $row['address']['lastname'];
  $postcode = $row['address']['postcode'];
  $street = $row['address']['street'];
  $city = $row['address']['city'];
  $telephone = $row['address']['telephone'];
  $country_id = $row['address']['country_id'];
  $address_type = $row['address']['address_type'];
  $company = $row['address']['company'];
  $country = $row['address']['country'];

  if (isset($id)) {
    $sql = "INSERT INTO `miletech` (`email`, `firstname`, `lastname`, `gender`, 
    `customer_activated`, `group_id`, `customer_company`, `default_billing`, `default_shipping`, 
    `is_active`, `created_at`, `updated_at`, `customer_invoice_email`,
    `customer_extra_text`, `customer_due_date_period`, `id`) VALUES (:email, :firstname, 
    :lastname, :gender, :customer_activated, :group_id, :customer_company, :default_billing, 
    :default_shipping, :is_active, :created_at, :updated_at, :customer_invoice_email, 
    :customer_extra_text, :customer_due_date_period, :id)";
    $stm_insert = $pdo->prepare($sql);
    $stm_insert->execute([
      'email' => $email,
      'firstname' => $firstname, 
      'lastname' => $lastname,
      'gender' => $gender,
      'customer_activated' => $customer_activated,
      'group_id' => $group_id,
      'customer_company' => $customer_company,
      'default_billing' => $default_billing,
      'default_shipping' => $default_shipping,
      'is_active' => $is_active,
      'created_at' => $created_at,
      'updated_at' => $updated_at,
      'customer_invoice_email' => $customer_invoice_email,
      'customer_extra_text' => $customer_extra_text,
      'customer_due_date_period' => $customer_due_date_period,
      'id' => $id
    ]);
  }

  if (isset($address)) {
    $sql = "INSERT INTO `miletech_address` (`id`, `customer_id`, `customer_address_id`, `email`, 
    `firstname`, `lastname`, `postcode`, `street`, `city`, `telephone`, `country_id`, 
    `address_type`, `company`, `country`) VALUES (:id, :customer_id, :customer_address_id, :email, 
    :firstname, :lastname, :postcode, :street, :city, :telephone, :country_id, :address_type, 
    :company, :country)";
    $stm_insert = $pdo->prepare($sql);
    $stm_insert->execute([
      'id' => $address_id,
      'customer_id' => $customer_id, 
      'customer_address_id' => $customer_address_id,
      'email' => $address_email,
      'firstname' => $address_firstname,
      'lastname' => $address_lastname,
      'postcode' => $postcode,
      'street' => $street,
      'city' => $city,
      'telephone' => $telephone,
      'country_id' => $country_id,
      'address_type' => $address_type,
      'company' => $company,
      'country' => $country,
    ]);
  }
}