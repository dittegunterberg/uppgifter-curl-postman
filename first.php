<?php

$ch = curl_init("http://medieinstitutet.se/");
$fp = fopen("medieinstitutet.txt", "w");

curl_setopt($ch, CURLOPT_FILE, $fp);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_exec($ch);
curl_close($ch);
fclose($fp);

$words = [
    'webbutveckling',
    'utbildningar',
    'medieinstitutet',
];

$content = file_get_contents("medieinstitutet.txt");
$no_tags = strip_tags($content);

foreach ($words as $word) {
    $number = substr_count($content, $word);
    echo $word . " förekommer " . $number . " gånger.<br>";
}