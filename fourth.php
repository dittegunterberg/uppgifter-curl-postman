<?php

$ch = curl_init("http://medieinstitutet.se/");
$fp = fopen("medieinstitutet2.txt", "w");

curl_setopt($ch, CURLOPT_FILE, $fp);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_exec($ch);
curl_close($ch);
fclose($fp);

$text = file_get_contents("medieinstitutet2.txt");
$no_tags = strip_tags($text);

$words = [
    'webbutveckling',
    'utbildningar',
    'medieinstitutet',
];

function count_words($text, $words) {
    foreach ($words as $word) {
        $number = substr_count($text, $word);
        echo $word . " förekommer " . $number . " gånger.<br>";
    }
}

count_words($no_tags, $words);