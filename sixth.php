<?php

function text($url, $file) {

    $ch = curl_init($url);
    $fp = fopen($file, "w");

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_exec($ch);
    curl_close($ch);
    fclose($fp);

    $text = file_get_contents($file);
    $no_tags = strip_tags($text);

    return $no_tags;
}

function count_words($text, $words) {
    foreach ($words as $word) {
        $number = substr_count($text, $word);
        echo $word . " förekommer " . $number . " gånger.<br>";
    }
}

count_words(text("http://medieinstitutet.se/", "medieinstitutet3.txt"), ['webbutveckling','utbildningar', 'medieinstitutet']);